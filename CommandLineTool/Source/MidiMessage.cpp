//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Jack on 01/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <cmath>
#include <iostream>

MidiMessage::MidiMessage()
{
    number = 80;
    velocity = 0;
    channel = 0;
    std::cout << "Constructor\n";
}
MidiMessage::MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel)
{
    number = initialNoteNumber;
    velocity = initialVelocity;
    channel = initialChannel;
    std::cout << "Constructor\n";
}

MidiMessage::~MidiMessage()
{
    std::cout << "Destructor\n";
}
void MidiMessage::setNoteNumber(int newNoteNumber)
{
    if (newNoteNumber > 127 || newNoteNumber <0)
        return;
    number = newNoteNumber;
}
int MidiMessage::getNoteNumber() const
{
    return number;
}
float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (number-69) / 12.0);
}
void MidiMessage::setChannelNo (int value) //Mutator
{
    if (value >= 0 && value <=15)
        channel = value;
}
int MidiMessage::getChannelNo() const //Accessor
{
    return channel;
}
void MidiMessage::setVelocity (int value) //Mutator
{
    if (value >= 0 && value <=127)
        velocity = value;
}
int MidiMessage::getVelocity() const //Accessor
{
    return velocity;
}
float MidiMessage::getFloatVelocity() const //Accessor
{
    return velocity/127.0;
}
