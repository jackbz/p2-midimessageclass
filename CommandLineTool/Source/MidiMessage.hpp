//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Jack on 01/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

/**Class for storing and manipulating MIDI note messages*/
class MidiMessage
{
public:
    /**Constructor*/
    MidiMessage();
    
    /**Constructor*/
    MidiMessage(int initialNoteNumber, int initialVelocity, int initialChannel);
    
    /**Destructor*/
    ~MidiMessage();
    
    /**Sets the MIDI note number of the message*/
    void setNoteNumber(int newNoteNumber); //mutator
    
    /**Returns the note number of the message*/
    int getNoteNumber() const; //accessor
    
    /**Returns the MIDI note as a frequency*/
    float getMidiNoteInHertz() const; //accessor
    
    /**Sets Channel number of the message*/
    void setChannelNo (int value); //Mutator
    
    /**Returns the channel number of the message*/
    int getChannelNo() const; //Accessor
    
    /**Sets the velocity of the message*/
    void setVelocity (int value); //Mutator
   
    /**Returns the velocity of the message*/
    int getVelocity() const; //Accessor
    
    /**Returns the velocity as amplitude*/
    float getFloatVelocity() const; //Accessor
    

private:
    int number, velocity, channel;
};

#endif /* MidiMessage_hpp */
